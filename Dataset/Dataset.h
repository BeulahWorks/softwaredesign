/*
 * Dataset.h
 *
 *  Created on: Oct 4, 2019
 *      Author: guo80
 */

#ifndef DATASET_H_
#define DATASET_H_

#include <vector>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <iomanip>
using std::vector;
using std::cout;
using std::string;

namespace hw3 {

class Dataset {
public:
	Dataset();
	void print();
	vector<double> getTemperature();
	vector<double> getHumidity();
	vector<string> getPlay();
private:
	vector<double> temperature;
	vector<double> humidity;
	vector<string> play;
};
}

#endif /* DATASET_H_ */
